// https://github.com/b00f/gnome-trash/blob/main/src/confirmDialog.ts
'use strict';

const St = imports.gi.St;
const GObject = imports.gi.GObject;
const ModalDialog = imports.ui.modalDialog;
const Clutter = imports.gi.Clutter;

class Confirm_Dialog
	extends ModalDialog.ModalDialog {

	static {
		GObject.registerClass(this);
	}

	constructor(
		title,
		desc,
		okLabel,
		cancelLabel,
		callback) {
		super();

		let main_box = new St.BoxLayout({
			vertical: false
		});
		this.contentLayout.add_child(main_box);

		let message_box = new St.BoxLayout({
			vertical: true
		});
		main_box.add_child(message_box);

		let subject_label = new St.Label({
			style: 'font-weight: bold',
			x_align: Clutter.ActorAlign.CENTER,
			text: title
		});
		message_box.add_child(subject_label);

		let desc_label = new St.Label({
			style: 'padding-top: 12px',
			x_align: Clutter.ActorAlign.CENTER,
			text: desc
		});
		message_box.add_child(desc_label);

		this.setButtons([
			{
				label: cancelLabel,
				action: () => {
					this.close();
				},
				key: Clutter.Escape
			},
			{
				label: okLabel,
				action: () => {
					this.close();
					callback();
				}
			}
		]);
	}
}

var open = function openConfirmDialog(
	title,
	message,
	callback,
	okLabel = _("OK"),
	cancelLabel = _("Cancel")) {

	let dlg = new Confirm_Dialog(title, message,
		okLabel, cancelLabel, callback);
	dlg.open();
}